****************
jsonschema-pyref
****************

.. Include the README as the top-level introduction to the documentation,
.. starting at line 3 to skip the README's top-level header

.. mdinclude:: ../README.md
   :start-line: 3
   :end-line: 230


Full Documentation
==================

.. toctree::
   :maxdepth: 2

   api


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
